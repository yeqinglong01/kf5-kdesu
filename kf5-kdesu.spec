%global framework kdesu

Name:           kf5-%{framework}
Version:        5.55.0
Release:        1
Summary:        KDE Frameworks 5 Tier 3 integration with su
License:        LGPLv2+
URL:            https://cgit.kde.org/%{framework}.git

%global majmin %(echo %{version} | cut -d. -f1-2)
%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin}  kf5-rpm-macros >= %{majmin}  kf5-ki18n-devel >= %{majmin}  kf5-kcoreaddons-devel >= %{majmin}  kf5-kservice-devel >= %{majmin}  kf5-kpty-devel >= %{majmin}  libX11-devel  qt5-qtbase-devel

%description
KDE Frameworks 5 Tier 3 integration with su for elevated privileges.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}       kf5-kpty-devel >= %{majmin}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version}


%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{cmake_kf5} ..
popd
%make_build -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}
%find_lang kdesu5_qt --all-name
%ldconfig_scriptlets

%files -f kdesu5_qt.lang
%doc README.md
%license COPYING.LIB
%{_kf5_libdir}/libKF5Su.so.*
%{_kf5_libexecdir}/kdesu_stub
%attr(2755,root,nobody) %{_kf5_libexecdir}/kdesud

%files devel
%{_kf5_includedir}/kdesu_version.h
%{_kf5_includedir}/KDESu/
%{_kf5_libdir}/libKF5Su.so
%{_kf5_libdir}/cmake/KF5Su/
%{_kf5_archdatadir}/mkspecs/modules/qt_KDESu.pri


%changelog
* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler
